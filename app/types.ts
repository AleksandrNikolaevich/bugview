type StaticDeviceInfo = {
    platform: "ios" | "android" | "windows" | "macos" | "web",
    systemVersion: string,
    nativeVersion: string,
    appVersion: string,
    releaseDate: string,
    apiLevel: number,
    appName:string,
    brand: string,
    buildNumber: string,
    bundleId: string,
    operator: string,
    deviceId: string,
    deviceLocale: string,
    isTablet: boolean,
    deviceCountry: string,
    ip: string,
    uniqueDeviceId: string,
    screenSize: {width: number, height: number},
    pixelRatio: number
}

type PromiseDeviceInfo = {
    battaryLevel: number,
    locationEnabled: boolean,
    freeDiskStorage: number
}

export type TDeviceInfo = Partial<StaticDeviceInfo & PromiseDeviceInfo>;

export type TError = Partial<Error> & { type: "js" | "native" }


export enum EventType {
    image = 'image',
    request = 'request',
    response = 'response',
    touch = 'touch',
    navigate = 'navigate'
}

export type Event = {
    time: number,
    type: EventType,
    data: any
}

export type BugViewLog = {
    date: string,
    bugviewVersion: string,
    deviceInfo: TDeviceInfo,
    timeline: Event[],
    error?: TError,
    screen?: string
    appVersion?: string
}