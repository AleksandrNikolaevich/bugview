export function logHandler(handler: any){
    return new Proxy(handler, {
        apply: function(target, thisArg, argumentsList) {
            
            const result = target(...argumentsList);

            try{
                console.log(target.prototype.constructor.name);
            }catch(e){

            }
           
            console.log("arguments: ", argumentsList);
            console.log("result: ", result);
            return result;
        }
    });
}