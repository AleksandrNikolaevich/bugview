import React, { useState, useCallback, useRef } from 'react';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import Tabs from '@material-ui/core/Tabs';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import { makeStyles, Typography, IconButton } from '@material-ui/core';
import { BugViewLog } from '../types';
import LogView from './LogView';
import { useDropzone } from 'react-dropzone';
import DropArea from './DropArea';
import { logHandler } from '../utils/utils';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		flex: 1,
		flexDirection: 'column',
		overflow: 'hidden'
	},
	fab: {
		position: 'absolute',
		bottom: theme.spacing(2),
		right: theme.spacing(2)
	}
}));

function findNextIndex(length: number, removedIndex: number): number {
	if (length > 1) {
		if (removedIndex === 0) {
			return 0
		}

		if (removedIndex === length - 1) {
			return length - 2
		}

		return removedIndex - 1
	}
	return -1
}

export default function Home(): JSX.Element {
	const [tabs, setTabsState] = useState<Record<string, BugViewLog>>({});
	const [activeTab, changeActiveTab] = useState(0);
	const classes = useStyles();

	const inputFile = useRef<any>(null);

	const handleChange = (event: any, newValue: number) => {
		changeActiveTab(newValue);
	}

	const openDialog = () => {
		inputFile.current.click();
	};

	const changeFileInputHandler = (event: any) => {
		event.stopPropagation();
		event.preventDefault();
		openFile(event.target.files[0]);
	};

	const openFile = (file: any) => {

		if (Object.keys(tabs).includes(file.name)) {
			logHandler(changeActiveTab)(Object.keys(tabs).indexOf(file.name));
			return null;
		}

		let reader = new FileReader();
		reader.readAsText(file);

		reader.onload = () => {
			const log = JSON.parse(reader.result as any);
			setTabsState((tabs) => ({
				...tabs,
				[file.name]: log
			}));

			logHandler(changeActiveTab)(Object.keys(tabs).length);

			console.log(log);
		};

		reader.onerror = function () {
			console.log(reader.error);
		};
	};

	const onDrop = useCallback((acceptedFiles) => {
		// Do something with the files
		openFile(acceptedFiles[0]);
	}, [tabs]);


	const closeTab = (name: string) => (event: any) => {
		event.stopPropagation();
		event.preventDefault();
		const index = Object.keys(tabs).indexOf(name);
		if (!~index) return;

		const active = logHandler(findNextIndex)(Object.keys(tabs).length, index);

		changeActiveTab(active);
		setTabsState((tabs) => {
			const newTabs = { ...tabs };
			delete newTabs[name];
			return newTabs
		});
	}

	const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

	const tabsKeys = Object.keys(tabs);

	return (
		<div className={classes.root} {...getRootProps()}>
			<input
				{...getInputProps()}
				onClick={(e) => {
					e.stopPropagation();
					e.preventDefault();
				}}
			/>
			<AppBar position="static">
				{!!tabsKeys.length && (
					<Tabs value={activeTab} onChange={handleChange} aria-label="simple tabs example">
						{tabsKeys.map((key) => <Tab key={key} label={<>
							<Typography variant="caption">{key}</Typography>
							<IconButton style={{ position: "absolute", right: 0 }} onClick={closeTab(key)}>
								<CloseIcon color="action" />
							</IconButton>

						</>
						} />)}
					</Tabs>
				)}
			</AppBar>
			{!!tabsKeys[activeTab] && <LogView {...tabs[tabsKeys[activeTab]]} filename={tabsKeys[activeTab]} />}
			{isDragActive && <DropArea />}
			<Fab className={classes.fab} variant="extended" size="medium" color="primary" onClick={openDialog}>
				<AddIcon />
				open
				<input
					type="file"
					id="file"
					ref={inputFile}
					style={{ display: 'none' }}
					onChange={changeFileInputHandler}
				/>
			</Fab>
		</div>
	);
}
