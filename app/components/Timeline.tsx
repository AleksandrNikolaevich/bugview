import * as React from 'react';
import { Event, EventType } from '../types';
import Grid from '@material-ui/core/Grid';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import StopIcon from '@material-ui/icons/Stop';
import { Slider, IconButton } from '@material-ui/core';
import { logHandler } from '../utils/utils';
import RequestView from './RequestView';

const Timeline: React.FC<{ timeline: Event[], filename: string }> = ({ timeline: src, filename }) => {
	const [ timeline, setTimeline ] = React.useState<Event[]>([]);

	const [ timelineFrame, setTimelineFrameA ] = React.useState(0);
	const setTimelineFrame = logHandler(setTimelineFrameA);

	const timelineTimer = React.useRef<any>();
	React.useEffect(
		() => {
			setTimeline(
				src.map(function(item) {
					if (item.type === 'image') {
						return {
							...item,
							data: 'data:image/jpg;base64, ' + item.data
						};
					}
					return item;
				})
			);
		},
		[ src ]
	);

	const handleChange = (event: any, value: number | number[]) => {
		if (!Array.isArray(value)) {
			setTimelineFrame(value);
		}
	};

	const play = React.useCallback(
		() => {
			let indexEvent = 0;
			let time = timeline[0].time;

			const minTime = timeline[0].time;
			const maxTime = timeline[timeline.length - 1].time;

			let timerTouches: any;

			const EventContainerComponent = document.getElementById('events-container')!;
			EventContainerComponent.innerHTML = '';

			let mapRequests: Record<string, boolean> = {};

			timelineTimer.current = setInterval(() => {
				if (indexEvent > timeline.length - 1) {
					clearInterval(timelineTimer.current);
					return;
				}

				const event = timeline[indexEvent];
				// if (event.type === "image") {
				// 	ImageComponent.src = event.data;
				// }

				// if (event.type === "touch") {
				// 	clearTimeout(timerTouches)
				// 	if (event.data.eventType) {
				// 		cx.beginPath();
				// 		cx.arc(event.data.pageX, event.data.pageY, 10, 0, 2 * Math.PI, false);
				// 		cx.fillStyle = 'rgba(255, 0, 0, .3)';
				// 		cx.fill();
				// 	}

				// 	timerTouches = setTimeout(function () {
				// 		cx.clearRect(0, 0, 1000, 1000);
				// 	}.bind(this), 100)

				// }

				if (event.type === 'request' || event.type === 'response') {
					if (!mapRequests[event.type + event.data.id]) {
						EventContainerComponent.innerHTML =
							EventContainerComponent.innerHTML +
							"<code style='display: block'>" +
							event.type +
							': ' +
							JSON.stringify(event.data).substr(0, 100) +
							'</code>';
						mapRequests[event.type + event.data.id] = true;
					}
				}

				time = time + 10;
				const nextEvent = timeline[indexEvent + 1];
				if (!nextEvent) {
					clearInterval(timelineTimer.current);
				}
				if (nextEvent && nextEvent.time < time) {
					indexEvent++;
				}
				setTimelineFrame((time - minTime) / (maxTime - minTime) * 100);
			}, 10);
		},
		[ timeline ]
	);

	return (
		<Grid container spacing={3}>
			{/* <Grid item xs={4} /> */}
			<Grid item xs={12}>
				{timeline.filter((e) => e.type === EventType.response || e.type === EventType.request).map((e) => {
					return <RequestView key={`${e.time}-${filename}-${e.data.id}`} {...e} />;
				})}
			</Grid>

			{/* <Grid item xs={12}>
				<Grid container spacing={2} alignItems="center">
					<Grid item>
						<IconButton onClick={play}>
							<PlayArrowIcon />
						</IconButton>
					</Grid>
					<Grid item xs>
						<Slider value={timelineFrame} onChange={handleChange} />
					</Grid>
				</Grid>
			</Grid> */}
		</Grid>
	);
};

export default Timeline;

// const play = React.useCallback(
// 	() => {
// 		let indexEvent = 0;
// 		let time = timeline[0].time;

// 		const minTime = timeline[0].time;
// 		const maxTime = timeline[timeline.length - 1].time;

// 		let timerTouches: any;

// 		const EventContainerComponent = document.getElementById('events-container')!;
// 		EventContainerComponent.innerHTML = "";

// 		let mapRequests: Record<string, boolean> = {};

// 		timelineTimer.current = setInterval(() => {
// 			if (indexEvent > timeline.length - 1) {
// 				clearInterval(timelineTimer.current);
// 				return;
// 			}

// 			const event = timeline[indexEvent];
// 			// if (event.type === "image") {
// 			// 	ImageComponent.src = event.data;
// 			// }

// 			// if (event.type === "touch") {
// 			// 	clearTimeout(timerTouches)
// 			// 	if (event.data.eventType) {
// 			// 		cx.beginPath();
// 			// 		cx.arc(event.data.pageX, event.data.pageY, 10, 0, 2 * Math.PI, false);
// 			// 		cx.fillStyle = 'rgba(255, 0, 0, .3)';
// 			// 		cx.fill();
// 			// 	}

// 			// 	timerTouches = setTimeout(function () {
// 			// 		cx.clearRect(0, 0, 1000, 1000);
// 			// 	}.bind(this), 100)

// 			// }

// 			if (event.type === 'request' || event.type === 'response') {
// 				if (!mapRequests[event.type + event.data.id]) {
// 					EventContainerComponent.innerHTML =
// 						EventContainerComponent.innerHTML +
// 						"<code style='display: block'>" +
// 						event.type +
// 						': ' +
// 						JSON.stringify(event.data).substr(0, 100) +
// 						'</code>';
// 					mapRequests[event.type + event.data.id] = true;
// 				}
// 			}

// 			time = time + 10;
// 			const nextEvent = timeline[indexEvent + 1];
// 			if (!nextEvent) {
// 				clearInterval(timelineTimer.current);
// 			}
// 			if (nextEvent && nextEvent.time < time) {
// 				indexEvent++;
// 			}
// 			setTimelineFrame((time - minTime) / (maxTime  - minTime) * 100 );
// 		}, 10);
// 	},
// 	[ timeline ]
// );
