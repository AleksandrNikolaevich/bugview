import * as React from "react";
import { TDeviceInfo } from "../types";
import { Typography } from "@material-ui/core";

const DeviceInfo: React.FC<{ device: TDeviceInfo }> = ({ device }) => {
    return (
        <ReactJson src={{
            date,
            bugviewVersion,
            appVersion,
            screen
        }} />
        <Typography>
            
            {JSON.stringify(device)}
        </Typography>
    )
}

export default DeviceInfo;