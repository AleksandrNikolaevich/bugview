import * as React from 'react';
import { Typography } from '@material-ui/core';
import { TError } from '../types';

const ErrorView: React.FC<{ error: TError | undefined }> = ({ error }) => {
	if (!error) return null;
	return <Typography variant="body1">{error.message}</Typography>;
};

export default ErrorView;
