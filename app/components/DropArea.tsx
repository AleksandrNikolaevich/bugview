import * as React from "react";
import { makeStyles } from "@material-ui/core";


const useStyles = makeStyles((theme) => ({
	root: {
        position: "absolute",
        zIndex: 10000,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: "rgba(255, 255, 255, .8)"
    },
    area: {
        position: "absolute",
        zIndex: 10001,
        top: 20,
        left: 20,
        right: 20,
        bottom: 20,
        border: "4px solid rgba(20, 100, 255,.5)",
        borderRadius: 20,
        borderStyle: "dashed"
    }
}));

const DropArea: React.FC = () => {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.area}/>
        </div>
    )
}

export default DropArea;