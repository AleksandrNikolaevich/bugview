import * as React from 'react';
import { Accordion, AccordionSummary, AccordionDetails, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const LogItem: React.FC<{ title: string, expanded?: boolean }> = ({ title, children, expanded }) => {
	const [expand, setExpanded] = React.useState(expanded);

	const handleChange = (event: any, isExpanded: boolean) => {
		setExpanded(isExpanded);
	};

	return (
		<Accordion expanded={expand} onChange={handleChange}>
			<AccordionSummary expandIcon={<ExpandMoreIcon />}>
				<Typography>{title}</Typography>
			</AccordionSummary>
			<AccordionDetails style={{ flexDirection: "column" }}>
				{typeof children === 'string' && (
					<Typography>
						{children}
					</Typography>
				)}
				{typeof children !== 'string' && (
					<>
						{children}
					</>
				)}
			</AccordionDetails>
		</Accordion>
	);
};

export default LogItem;
