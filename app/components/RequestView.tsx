import * as React from 'react';
import { Typography, Grid, Chip, Accordion, AccordionSummary, AccordionDetails } from '@material-ui/core';
import { Event, EventType } from '../types';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ReactJson from 'react-json-view';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import moment from "moment";
import { green } from '@material-ui/core/colors';

const RequestView: React.FC<Event> = ({ time, data, type }) => {
	const [expanded, setExpanded] = React.useState(false);

	const handleChange = (event: any, isExpanded: boolean) => {
		setExpanded(isExpanded);
	};
	const Icon = type === EventType.response ? <ArrowDownwardIcon style={{ color: green[500] }}/> : <ArrowUpwardIcon color="secondary"/>;

	const status =
		type === EventType.response ? (
			<Chip
				variant="outlined"
				color={data.status >= 400 ? 'secondary' : 'primary'}
				size="small"
				label={data.status}
				style={{ marginLeft: 20}}
			/>
		) : <div />;

	const date = moment(time).format("DD.MM.YYYY HH:mm:ss");

	return (
		<Accordion expanded={expanded} onChange={handleChange}>
			<AccordionSummary
				expandIcon={<ExpandMoreIcon />}
				aria-controls="panel1a-content"
				id="panel1a-header"
			>
				{Icon}<Typography color="primary" variant="body1" component="span" style={{ marginRight: 20}}>{date} </Typography> <Typography variant="body1" component="span">  {data.method}: {data.url.slice(0, 100)}</Typography>{status}
			</AccordionSummary>
			<AccordionDetails>
				{
					expanded &&
					<ReactJson src={data} />
				}

			</AccordionDetails>
		</Accordion>


	);
};

export default RequestView;
