import { Container, Typography } from '@material-ui/core';
import * as React from 'react';
import ReactJson from 'react-json-view';
import { BugViewLog } from '../types';
import ErrorView from './ErrorView';
import LogItem from './LogItem';
import Timeline from './Timeline';

const LogView: React.FC<BugViewLog & { filename: string }> = ({ error, deviceInfo, date, bugviewVersion, appVersion, screen, timeline, filename }) => {

	return (
		<Container style={{ overflow: 'auto', paddingTop: 40, paddingBottom: 40 }}>

			{!!error && (
				<LogItem title={`Ошибка`} expanded>
					<Typography>{`тип ошибки: ${error.type}`}</Typography>
					<ErrorView error={error} />
				</LogItem>
			)}
			{!!error && (
				<LogItem title={'Стэк'}>
					<Typography>{error.stack}</Typography>
				</LogItem>
			)}
			<Typography style={{ marginTop: 40, marginBottom: 10 }} variant="h6">Запросы </Typography>

			<Timeline timeline={timeline} filename={filename}/>

				< Typography style={{ marginTop: 40, marginBottom: 10 }} variant="h6">Другое </Typography>

			<LogItem title={'Устройство'}>
				{/* <DeviceInfo device={deviceInfo} /> */}
				<ReactJson src={deviceInfo} />
			</LogItem>


			<LogItem title={'Дополнительная информация'}>
				<ReactJson src={{
					date,
					bugviewVersion,
					appVersion,
					screen
				}} />
			</LogItem>
		</Container>
	);
};

export default LogView;
